package me.nuf.subjectapi;

/**
 * Created by nuf on 2/27/2016.
 */
public class Subject {

    private boolean canceled = false;

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }
}
