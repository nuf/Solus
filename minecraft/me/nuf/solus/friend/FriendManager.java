package me.nuf.solus.friend;

import me.nuf.api.management.ListManager;

import java.util.ArrayList;

/**
 * Created by nuf on 3/20/2016.
 */
public final class FriendManager extends ListManager<Friend> {
    public FriendManager() {
        elements = new ArrayList<>();
    }

    public boolean isFriend(String label) {
        for (Friend friend : elements)
            if (label.equalsIgnoreCase(friend.getLabel()) || label.equalsIgnoreCase(friend.getAlias()))
                return true;
        return false;
    }

    public String replace(String text) {
        for (Friend friend : elements)
            if (text.contains(friend.getLabel()))
                text = text.replace(friend.getLabel(), String.format("\247c%s\247r", friend.getAlias()));
        return text;
    }
}
