package me.nuf.solus.properties;

/**
 * Created by nuf on 3/19/2016.
 */
public class NumberProperty<T extends Number> extends Property<T> {

    private final T minimum, maximum;

    private boolean clamp;

    public NumberProperty(T value, T minimum, T maximum, String... aliases) {
        super(value, aliases);
        clamp = true;
        this.minimum = minimum;
        this.maximum = maximum;
    }
    public NumberProperty(T value, String... aliases) {
        super(value, aliases);
        clamp = false;
        this.minimum = maximum = null;
    }

    public final T getMaximum() {
        return maximum;
    }

    public final T getMinimum() {
        return minimum;
    }

    @Override
    public void setValue(T value) {
        if (clamp) {
            Number number = value;
        }
        this.value = value;//TODO clamping
    }
}
