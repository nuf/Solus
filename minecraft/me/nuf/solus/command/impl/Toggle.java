package me.nuf.solus.command.impl;

import me.nuf.api.interfaces.Toggleable;
import me.nuf.solus.command.Argument;
import me.nuf.solus.command.Command;
import me.nuf.solus.core.Solus;
import me.nuf.solus.module.Module;
import me.nuf.solus.module.ToggleableModule;

/**
 * Created by nuf on 3/20/2016.
 */
public final class Toggle extends Command {
    public Toggle() {
        super(new Argument[]{new Argument(String.class, "module")}, "toggle", "t");
    }

    @Override
    public String dispatch() {
        String moduleInput = getArgument("module").getValue();
        Module module = Solus.getInstance().getModuleManager().getUsingLabel(moduleInput);

        if (module == null)
            return "That module does not exist.";

        if (!(module instanceof Toggleable))
            return "That module is not toggleable.";

        ToggleableModule toggleableModule = (ToggleableModule) module;
        toggleableModule.toggle();
        return String.format("%s %s been %s&7.", toggleableModule.getAliases()[0], toggleableModule.getAliases()[0].endsWith("s") ? "have" : "has", toggleableModule.isEnabled() ? "&aenabled" : "&cdisabled");
    }
}
