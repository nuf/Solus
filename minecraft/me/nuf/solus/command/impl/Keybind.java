package me.nuf.solus.command.impl;

import me.nuf.api.interfaces.Toggleable;
import me.nuf.solus.command.Argument;
import me.nuf.solus.command.Command;
import me.nuf.solus.core.Solus;
import me.nuf.solus.macro.Macro;
import me.nuf.solus.module.Module;
import me.nuf.solus.module.ToggleableModule;
import org.lwjgl.input.Keyboard;

/**
 * Created by nuf on 3/20/2016.
 */
public final class Keybind extends Command {
    public Keybind() {
        super(new Argument[]{new Argument(String.class, "action"), new Argument(String.class, "label"), new Argument(String.class, "key")}, "keybind", "bind", "kb");
    }

    @Override
    public String dispatch() {
        String action = getArgument("action").getValue();
        String label = getArgument("label").getValue();
        String key = getArgument("key").getValue().toUpperCase();

        if (action.equalsIgnoreCase("module")) {
            Module module = Solus.getInstance().getModuleManager().getUsingLabel(label);

            if (module == null)
                return "That module does not exist.";

            if (!(module instanceof Toggleable))
                return "That module is not toggleable.";

            ToggleableModule toggleableModule = (ToggleableModule) module;
            try {
                if (!key.equalsIgnoreCase("none")) {
                    Solus.getInstance().getKeybindManager().getUsingLabel(String.format("%sToggle", label)).setKey(Keyboard.getKeyIndex(key));
                } else {
                    Solus.getInstance().getKeybindManager().getUsingLabel(String.format("%sToggle", label)).setKey(Keyboard.KEY_NONE);
                }
            } catch (Exception e) {
                return "The fuck happened.";
            }
            return String.format("%s keybind has been set to %s.", toggleableModule.getAliases()[0], Keyboard.getKeyName(Keyboard.getKeyIndex(key)));
        } else if (action.equalsIgnoreCase("macro")) {
            if (Solus.getInstance().getMacroManager().isMacro(Keyboard.getKeyIndex(key))) {
                Solus.getInstance().getMacroManager().remove(Keyboard.getKeyIndex(key));
                return String.format("Removed macro with the keybind %s.", Keyboard.getKeyName(Keyboard.getKeyIndex(key)));
            }

            Solus.getInstance().getMacroManager().register(new Macro(Keyboard.getKeyIndex(key), label));
            return String.format("Added a macro with the keybind %s.", Keyboard.getKeyName(Keyboard.getKeyIndex(key)));
        } else {
            return "Invalid action.";
        }
    }
}
