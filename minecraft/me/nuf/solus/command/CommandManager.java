package me.nuf.solus.command;

import me.nuf.api.management.ListManager;
import me.nuf.solus.command.impl.Keybind;
import me.nuf.solus.command.impl.Toggle;
import me.nuf.solus.core.Solus;
import me.nuf.solus.module.Module;
import me.nuf.solus.printing.Printer;
import me.nuf.solus.properties.EnumProperty;
import me.nuf.solus.properties.Property;
import me.nuf.solus.subjects.PacketSubject;
import me.nuf.subjectapi.Listener;
import net.minecraft.network.play.client.CPacketChatMessage;

import java.util.ArrayList;
import java.util.StringJoiner;

/**
 * Created by nuf on 3/20/2016.
 */
public final class CommandManager extends ListManager<Command> {

    private String prefix = "-";

    public CommandManager() {
        elements = new ArrayList<>();
        register(new Toggle());
        register(new Keybind());

        elements.sort((cmd1, cmd2) -> cmd1.getAliases()[0].compareTo(cmd2.getAliases()[0]));
        Solus.getInstance().getSubjectManager().register(new Listener<PacketSubject>("solus_main_command_packet_listener") {
            @Override
            public void call(PacketSubject subject) {
                if (subject.getPacket() instanceof CPacketChatMessage) {
                    CPacketChatMessage packet = (CPacketChatMessage) subject.getPacket();
                    String message = packet.getMessage().trim();
                    if (message.startsWith(prefix)) {
                        subject.setCanceled(true);

                        boolean exists = false;

                        String[] arguments = message.split(" ");

                        if (message.length() < 1) {
                            Printer.getPrinter().printToChat("No command was entered.");
                            return;
                        }

                        String execute = message.contains(" ") ? arguments[0] : message;
                        for (Command command : getElements())
                            for (String alias : command.getAliases())
                                if (execute.replace(getPrefix(), "").equalsIgnoreCase(alias.replaceAll(" ", ""))) {
                                    exists = true;
                                    try {
                                        Printer.getPrinter().printToChat(command.dispatch(arguments));
                                    } catch (Exception e) {
                                        Printer.getPrinter().printToChat(
                                                String.format("%s%s %s", prefix, alias, command.getSyntax()));
                                    }
                                }

                        String[] argz = message.split(" ");
                        for (Module mod : Solus.getInstance().getModuleManager().getElements()) {
                            for (String alias : mod.getAliases()) {
                                try {
                                    if (argz[0].equalsIgnoreCase(getPrefix() + alias.replace(" ", ""))) {
                                        exists = true;//TODO dubling
                                        String propertyName = argz[1];
                                        if (argz[1].equalsIgnoreCase("list")) {
                                            if (mod.getProperties().size() > 0) {
                                                StringJoiner stringJoiner = new StringJoiner(", ");
                                                for (Property property : mod.getProperties())
                                                    stringJoiner.add(String.format("%s&e[%s]&7",
                                                            property.getAliases()[0], property.getValue()));
                                                Printer.getPrinter()
                                                        .printToChat(String.format("Properties (%s): %s.",
                                                                mod.getProperties().size(), stringJoiner.toString()));
                                            } else {
                                                Printer.getPrinter().printToChat(String
                                                        .format("&e%s&7 has no properties.", mod.getAliases()[0]));
                                            }
                                        } else {
                                            Property property = mod.getUsingLabel(propertyName);
                                            if (property != null) {
                                                if (property.getValue() instanceof Number) {
                                                    if (property.getValue() instanceof Double)
                                                        property.setValue(Double.parseDouble(argz[2]));
                                                    if (property.getValue() instanceof Integer)
                                                        property.setValue(Integer.parseInt(argz[2]));
                                                    if (property.getValue() instanceof Float)
                                                        property.setValue(Float.parseFloat(argz[2]));
                                                    if (property.getValue() instanceof Long)
                                                        property.setValue(Long.parseLong(argz[2]));
                                                    Printer.getPrinter()
                                                            .printToChat(String.format(
                                                                    "&e%s&7 has been set to &e%s&7 for %s.",
                                                                    property.getAliases()[0], property.getValue(),
                                                                    mod.getAliases()[0]));
                                                } else if (property.getValue() instanceof Enum) {
                                                    if (!argz[2].equalsIgnoreCase("list")) {
                                                        ((EnumProperty) property).setViaString(argz[2]);
                                                        Printer.getPrinter()
                                                                .printToChat(String.format(
                                                                        "&e%s&7 has been set to &e%s&7 for %s.",
                                                                        property.getAliases()[0], property.getValue(),
                                                                        mod.getAliases()[0]));
                                                    } else {
                                                        StringJoiner stringJoiner = new StringJoiner(", ");
                                                        Enum[] array;
                                                        for (int length = (array = (Enum[]) ((property)
                                                                .getValue()).getClass()
                                                                .getEnumConstants()).length, i = 0; i < length; i++)
                                                            stringJoiner.add(String.format("%s%s&7",
                                                                    array[i].name().equalsIgnoreCase(
                                                                            property.getValue().toString()) ? "&a"
                                                                            : "&c",
                                                                    array[i].name()));
                                                        Printer.getPrinter()
                                                                .printToChat(String.format("Modes (%s): %s.",
                                                                        array.length, stringJoiner.toString()));
                                                    }
                                                } else if (property.getValue() instanceof String) {
                                                    property.setValue(argz[2]);
                                                    Printer.getPrinter()
                                                            .printToChat(String.format(
                                                                    "&e%s&7 has been set to &e\"%s\"&7 for %s.",
                                                                    property.getAliases()[0], property.getValue(),
                                                                    mod.getAliases()[0]));
                                                } else if (property.getValue() instanceof Boolean) {
                                                    property.setValue(!(Boolean) property.getValue());
                                                    Printer.getPrinter().printToChat(String.format(
                                                            "&e%s&7 has been %s&7 for %s.", property.getAliases()[0],
                                                            (Boolean) property.getValue() ? "&aenabled" : "&cdisabled",
                                                            mod.getAliases()[0]));
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        if (!exists)
                            Printer.getPrinter()
                                    .printToChat("Invalid command.");
                    }
                }
            }
        });
    }

    public String getPrefix() {
        return prefix;
    }
}
