package me.nuf.solus.printing;

import me.nuf.api.interfaces.Printable;
import me.nuf.solus.core.Solus;
import net.minecraft.client.Minecraft;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

import java.util.logging.Level;

/**
 * Created by nuf on 3/19/2016.
 */
public final class Printer implements Printable {

    private static final Printer PRINTER = new Printer();

    @Override
    public void print(Level level, String message) {
        System.out.println(String.format("[%s][%s] %s", Solus.CLIENT_TITLE, level.toString(), message));
    }

    @Override
    public void printToChat(String message) {
        if (Minecraft.getMinecraft().thePlayer != null)
            Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString(String.format("\247c[%s] \2477%s", Solus.CLIENT_TITLE, message.replace("&", "\247"))).setChatStyle(new Style().setColor(TextFormatting.GRAY)));
    }

    public static final Printer getPrinter() {
        return PRINTER;
    }
}
