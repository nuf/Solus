package me.nuf.solus.keybind.actions;

import me.nuf.solus.keybind.Action;
import me.nuf.solus.module.ToggleableModule;

/**
 * Created by nuf on 3/19/2016.
 */
public class ModuleToggleAction extends Action {

    private final ToggleableModule toggleableModule;

    public ModuleToggleAction(ToggleableModule toggleableModule) {
        this.toggleableModule = toggleableModule;
    }

    @Override
    public void dispatch() {
        toggleableModule.toggle();
    }

    public final ToggleableModule getToggleableModule() {
        return toggleableModule;
    }
}
