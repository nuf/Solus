package me.nuf.solus.keybind;

/**
 * Created by nuf on 3/19/2016.
 */
public abstract class Action {

    public abstract void dispatch();

}
