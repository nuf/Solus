package me.nuf.solus.core;

import me.nuf.solus.command.CommandManager;
import me.nuf.solus.friend.FriendManager;
import me.nuf.solus.keybind.KeybindManager;
import me.nuf.solus.macro.MacroManager;
import me.nuf.solus.module.ModuleManager;
import me.nuf.solus.printing.Printer;
import me.nuf.subjectapi.Listener;
import me.nuf.subjectapi.basic.BasicSubjectManager;
import me.nuf.subjectapi.events.system.ShutdownSubject;
import me.nuf.subjectapi.events.system.StartupSubject;
import org.lwjgl.opengl.Display;

import java.io.File;
import java.util.logging.Level;

/**
 * Created by nuf on 3/19/2016.
 */
public final class Solus {

    private static Solus instance;

    public static final String CLIENT_TITLE = "Solus";
    public static final int CLIENT_VERSION = 1;

    private final File directory;

    private final BasicSubjectManager subjectManager;
    private final ModuleManager moduleManager;
    private final KeybindManager keybindManager;
    private final MacroManager macroManager;
    private final CommandManager commandManager;
    private final FriendManager friendManager;

    public Solus() {
        Printer.getPrinter().print(Level.INFO, "Initiated client startup.");

        instance = this;

        directory = new File(System.getProperty("user.home"), CLIENT_TITLE);
        if (!directory.exists())
            Printer.getPrinter().print(Level.INFO, String.format("%s client directory.", directory.mkdir() ? "Created" : "Failed to create"));

        subjectManager = new BasicSubjectManager();
        keybindManager = new KeybindManager();
        moduleManager = new ModuleManager();
        macroManager = new MacroManager();
        commandManager = new CommandManager();
        friendManager = new FriendManager();

        getSubjectManager().register(new Listener<StartupSubject>("solus_main_startup_listener") {
            @Override
            public void call(StartupSubject subject) {
            }
        });

        getSubjectManager().register(new Listener<ShutdownSubject>("solus_main_shutdown_listener") {
            @Override
            public void call(ShutdownSubject subject) {
            }
        });

        Display.setTitle(String.format("%s build-%s", CLIENT_TITLE, CLIENT_VERSION));

        Printer.getPrinter().print(Level.INFO, "Finished client startup.");
    }

    public static Solus getInstance() {
        return instance;
    }

    public final BasicSubjectManager getSubjectManager() {
        return subjectManager;
    }

    public final ModuleManager getModuleManager() {
        return moduleManager;
    }

    public final KeybindManager getKeybindManager() {
        return keybindManager;
    }

    public final MacroManager getMacroManager() {
        return macroManager;
    }

    public final CommandManager getCommandManager() {
        return commandManager;
    }

    public final FriendManager getFriendManager() {
        return friendManager;
    }

    public final File getDirectory() {
        return directory;
    }

}
