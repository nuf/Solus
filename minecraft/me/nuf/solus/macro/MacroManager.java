package me.nuf.solus.macro;

import me.nuf.api.management.ListManager;
import me.nuf.solus.core.Solus;
import me.nuf.solus.subjects.ActionSubject;
import me.nuf.subjectapi.Listener;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;

/**
 * Created by nuf on 3/20/2016.
 */
public final class MacroManager extends ListManager<Macro> {
    public MacroManager() {
        elements = new ArrayList<>();
        Solus.getInstance().getSubjectManager().register(new Listener<ActionSubject>("solus_main_macro_action_listener") {
            @Override
            public void call(ActionSubject subject) {
                if (subject.getType() == ActionSubject.Type.KEY_PRESS)
                    getElements().forEach(macro -> {
                        if (macro.getKey() != Keyboard.KEY_NONE && macro.getKey() == subject.getKey())
                            macro.dispatch();
                    });
            }
        });
    }

    public Macro getUsingKey(int key) {
        for (Macro macro : elements)
            if (key == macro.getKey())
                return macro;
        return null;
    }

    public boolean isMacro(int key) {
        for (Macro macro : elements)
            if (key == macro.getKey())
                return true;
        return false;
    }

    public void remove(int key) {
        Macro macro = getUsingKey(key);
        if (macro != null)
            elements.remove(macro);
    }
}
