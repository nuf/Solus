package me.nuf.solus.subjects;

import me.nuf.subjectapi.Subject;

/**
 * Created by nuf on 3/19/2016.
 */
public class MotionUpdateSubject extends Subject {

    private final Time time;

    private float rotationYaw, rotationPitch, oldRotationYaw, oldRotationPitch;

    public MotionUpdateSubject(Time time, float rotationYaw, float rotationPitch) {
        this.time = time;
        this.rotationYaw = oldRotationYaw = rotationYaw;
        this.rotationPitch = oldRotationPitch = rotationPitch;
    }

    public MotionUpdateSubject(Time time) {
        this.time = time;
    }

    public float getRotationYaw() {
        return rotationYaw;
    }

    public void setRotationYaw(float rotationYaw) {
        this.rotationYaw = rotationYaw;
    }

    public float getRotationPitch() {
        return rotationPitch;
    }

    public void setRotationPitch(float rotationPitch) {
        this.rotationPitch = rotationPitch;
    }

    public float getOldRotationYaw() {
        return oldRotationYaw;
    }

    public void setOldRotationYaw(float oldRotationYaw) {
        this.oldRotationYaw = oldRotationYaw;
    }

    public float getOldRotationPitch() {
        return oldRotationPitch;
    }

    public void setOldRotationPitch(float oldRotationPitch) {
        this.oldRotationPitch = oldRotationPitch;
    }


    public Time getTime() {
        return time;
    }

    public enum Time {
        PRE, POST
    }

}
