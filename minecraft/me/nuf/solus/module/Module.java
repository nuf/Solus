package me.nuf.solus.module;

import me.nuf.solus.properties.Property;
import net.minecraft.client.Minecraft;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuf on 3/19/2016.
 */
public class Module {

    private final String[] aliases;

    private final List<Property> properties = new ArrayList<>();

    protected Minecraft minecraft = Minecraft.getMinecraft();

    public Module(String... aliases) {
        this.aliases = aliases;
    }

    public final String[] getAliases() {
        return aliases;
    }

    public final List<Property> getProperties() {
        return properties;
    }

    protected void offerProperties(Property... properties) {
        for (Property property : properties)
            this.properties.add(property);
    }

    public Property getUsingLabel(String label) {
        for (Property property : properties)
            for (String alias : property.getAliases())
                if (label.equalsIgnoreCase(alias))
                    return property;
        return null;
    }
}
