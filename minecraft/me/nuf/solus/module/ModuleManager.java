package me.nuf.solus.module;

import me.nuf.api.management.ListManager;
import me.nuf.solus.core.Solus;
import me.nuf.solus.module.impl.active.HUD;
import me.nuf.solus.module.impl.combat.Criticals;
import me.nuf.solus.module.impl.combat.KillAura;
import me.nuf.solus.module.impl.exploits.FastConsume;
import me.nuf.solus.module.impl.exploits.Regen;
import me.nuf.solus.module.impl.movement.NoSlow;
import me.nuf.solus.module.impl.movement.Speed;
import me.nuf.solus.module.impl.render.NameProtect;
import me.nuf.solus.subjects.ActionSubject;
import me.nuf.subjectapi.Listener;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;

/**
 * Created by nuf on 3/19/2016.
 */
public final class ModuleManager extends ListManager<Module> {

    public ModuleManager() {
        elements = new ArrayList<>();
        register(new HUD());
        register(new Speed());
        register(new KillAura());
        register(new NameProtect());
        register(new Regen());
        register(new NoSlow());
        register(new Criticals());
        register(new FastConsume());

        elements.sort((mod1, mod2) -> mod1.getAliases()[0].compareTo(mod2.getAliases()[0]));

        Solus.getInstance().getSubjectManager().register(new Listener<ActionSubject>("solus_main_keybind_action_listener") {
            @Override
            public void call(ActionSubject subject) {
                if (subject.getType() == ActionSubject.Type.KEY_PRESS)
                    Solus.getInstance().getKeybindManager().getElements().forEach(keybind -> {
                        if (keybind.getKey() != Keyboard.KEY_NONE && subject.getKey() == keybind.getKey())
                            keybind.getAction().dispatch();
                    });
            }
        });
    }

    public Module getUsingLabel(String label) {
        for (Module module : elements)
            for (String alias : module.getAliases())
                if (label.equalsIgnoreCase(alias.replace(" ", "")))
                    return module;
        return null;
    }
}
