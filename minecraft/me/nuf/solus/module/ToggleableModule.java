package me.nuf.solus.module;

import me.nuf.api.interfaces.Toggleable;
import me.nuf.solus.core.Solus;
import me.nuf.solus.keybind.Keybind;
import me.nuf.solus.keybind.actions.ModuleToggleAction;
import me.nuf.subjectapi.Listener;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nuf on 3/19/2016.
 */
public class ToggleableModule extends Module implements Toggleable {

    private final List<Listener> listeners = new ArrayList<>();

    private boolean enabled;
    private boolean drawn;

    private final int color;

    private final Category category;

    public ToggleableModule(String[] aliases, boolean drawn, int color, Category category) {
        super(aliases);
        this.color = color;
        this.category = category;
        this.drawn = drawn;
        Solus.getInstance().getKeybindManager().register(new Keybind(String.format("%sToggle", getAliases()[0].toLowerCase().replace(" ", "")), new ModuleToggleAction(this), Keyboard.KEY_NONE));
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;

        if (this.enabled) {
            onEnable();
        } else {
            onDisable();
        }
    }

    @Override
    public void toggle() {
        setEnabled(!enabled);
    }

    public final int getColor() {
        return color;
    }

    public final Category getCategory() {
        return category;
    }

    public boolean isDrawn() {
        return drawn;
    }

    public void setDrawn(boolean drawn) {
        this.drawn = drawn;
    }

    protected void onEnable() {
        listeners.forEach(listener -> Solus.getInstance().getSubjectManager().register(listener));
    }

    protected void onDisable() {
        listeners.forEach(listener -> Solus.getInstance().getSubjectManager().unregister(listener));
    }

    protected void addListeners(Listener... listeners) {
        for (Listener listener : listeners)
            this.listeners.add(listener);
    }
}
