package me.nuf.solus.module.impl.active;

import me.nuf.api.interfaces.Toggleable;
import me.nuf.solus.core.Solus;
import me.nuf.solus.module.Module;
import me.nuf.solus.module.ToggleableModule;
import me.nuf.solus.properties.Property;
import me.nuf.solus.subjects.GameOverlaySubject;
import me.nuf.subjectapi.Listener;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.renderer.GlStateManager;
import org.lwjgl.opengl.GL11;

import java.util.List;

/**
 * Created by nuf on 3/19/2016.
 */
public final class HUD extends Module {

    private final Property<Boolean> watermark = new Property<>(true, "Watermark", "wm"), durability = new Property<>(true, "Durability", "dura", "d"), coordinates = new Property<>(true, "Coords", "coord", "c"), arraylist = new Property<>(true, "ArrayList", "al", "array", "list");

    public HUD() {
        super("HUD", "overlay");
        this.offerProperties(watermark, coordinates, durability, arraylist);
        Solus.getInstance().getSubjectManager().register(new Listener<GameOverlaySubject>("hud_game_overlay_listener") {
            @Override
            public void call(GameOverlaySubject subject) {
                if (minecraft.gameSettings.showDebugInfo)
                    return;
                if (watermark.getValue())
                    renderWatermark();
                if (arraylist.getValue())
                    renderArrayList();
                if (coordinates.getValue())
                    renderCoordinates();
                if (durability.getValue())
                    renderDurability();
            }
        });
    }

    private void renderWatermark() {
        GlStateManager.pushMatrix();
        minecraft.fontRenderer.drawStringWithShadow(String.format("%s b%s", Solus.CLIENT_TITLE, Solus.CLIENT_VERSION), 2, 2, 0xFFAAAAAA);
        GlStateManager.popMatrix();
    }

    private void renderDurability() {
        if (minecraft.thePlayer.inventory.getCurrentItem() != null)
            if (minecraft.thePlayer.inventory.getCurrentItem().isItemStackDamageable()) {
                int damage = minecraft.thePlayer.inventory.getCurrentItem().getMaxDamage() - minecraft.thePlayer.inventory.getCurrentItem().getItemDamage();
                GlStateManager.pushMatrix();
                GL11.glScalef(0.95F, 0.95F, 0.95F);
                minecraft.fontRenderer.drawStringWithShadow(String.format("{%s}", Integer.toString(damage)), 2, watermark.getValue() ? 12 : 2, 0xFFAAAAAA);
                GlStateManager.popMatrix();
            }
    }

    private void renderArrayList() {
        List<Module> modules = Solus.getInstance().getModuleManager().getElements();

        modules.sort((mod1, mod2) -> minecraft.fontRenderer.getStringWidth(mod2.getAliases()[0]) - minecraft.fontRenderer.getStringWidth(mod1.getAliases()[0]));

        int posY = -7;
        for (Module module : modules)
            if (module instanceof Toggleable) {
                ToggleableModule toggleableModule = (ToggleableModule) module;
                if (toggleableModule.isEnabled() && toggleableModule.isDrawn()) {
                    int tagWidth = minecraft.fontRenderer.getStringWidth(toggleableModule.getAliases()[0]);
                    minecraft.fontRenderer.drawStringWithShadow(toggleableModule.getAliases()[0], (minecraft.displayWidth / 2 - tagWidth) - 2, posY += 9, toggleableModule.getColor());
                }
            }
    }

    private void renderCoordinates() {
        minecraft.fontRenderer.drawStringWithShadow(String.format("XYZ {%s, %s, %s}", (int) minecraft.thePlayer.posX, (int) minecraft.thePlayer.posY, (int) minecraft.thePlayer.posZ), 2, minecraft.displayHeight / 2 - (minecraft.currentScreen instanceof GuiChat ? 23 : 9), 0xFFAAAAAA);
    }
}
