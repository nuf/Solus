package me.nuf.solus.module.impl.combat;

import me.nuf.solus.module.Category;
import me.nuf.solus.module.ToggleableModule;
import me.nuf.solus.subjects.PacketSubject;
import me.nuf.subjectapi.Listener;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.client.CPacketUseEntity;

/**
 * Created by nuf on 3/21/2016.
 */
public final class Criticals extends ToggleableModule {
    private boolean next = false;

    public Criticals() {
        super(new String[]{"Criticals", "critical", "crit", "crits"}, true, 0xFFB3A389, Category.COMBAT);
        this.addListeners(new Listener<PacketSubject>("criticals_packet_listener") {
            @Override
            public void call(PacketSubject subject) {
                if (subject.getPacket() instanceof CPacketUseEntity) {
                    CPacketUseEntity packet = (CPacketUseEntity) subject.getPacket();
                    next = !next;
                    if (next && shouldCritical() && packet.getAction() == CPacketUseEntity.Action.ATTACK) {
                        minecraft.thePlayer.sendQueue.addToSendQueue(new CPacketPlayer.C04PacketPlayerPosition(minecraft.thePlayer.posX, minecraft.thePlayer.posY + 0.05F, minecraft.thePlayer.posZ, false));
                        minecraft.thePlayer.sendQueue.addToSendQueue(new CPacketPlayer.C04PacketPlayerPosition(minecraft.thePlayer.posX, minecraft.thePlayer.posY, minecraft.thePlayer.posZ, false));
                        minecraft.thePlayer.sendQueue.addToSendQueue(new CPacketPlayer.C04PacketPlayerPosition(minecraft.thePlayer.posX, minecraft.thePlayer.posY + 0.012511F, minecraft.thePlayer.posZ, false));
                        minecraft.thePlayer.sendQueue.addToSendQueue(new CPacketPlayer.C04PacketPlayerPosition(minecraft.thePlayer.posX, minecraft.thePlayer.posY, minecraft.thePlayer.posZ, false));
                    }
                }
            }
        });
    }

    @Override
    protected void onDisable() {
        super.onDisable();
        next = false;
    }

    private boolean shouldCritical() {
        return minecraft.thePlayer.onGround && minecraft.thePlayer.isCollidedVertically && !minecraft.thePlayer.isInLava() && !minecraft.thePlayer.isInWater();
    }
}
