package me.nuf.solus.module.impl.combat;

import me.nuf.api.stopwatch.Stopwatch;
import me.nuf.solus.core.Solus;
import me.nuf.solus.module.Category;
import me.nuf.solus.module.ToggleableModule;
import me.nuf.solus.properties.NumberProperty;
import me.nuf.solus.properties.Property;
import me.nuf.solus.subjects.MotionUpdateSubject;
import me.nuf.subjectapi.Listener;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.MathHelper;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by nuf on 3/19/2016.
 */
public final class KillAura extends ToggleableModule {

    private final Stopwatch stopwatch = new Stopwatch();

    private EntityLivingBase target = null;

    private final List<EntityLivingBase> entities = new CopyOnWriteArrayList<>();

    private final Property<Boolean> friendProtect = new Property<>(true, "Friend-Protect", "protect", "friendprotect", "friend", "fp"), player = new Property<>(true, "Players", "player", "p"), lockview = new Property<>(false, "Lockview", "lv"), monsters = new Property<>(false, "Monsters", "monster", "mon"), animals = new Property<>(false, "Animals", "ani", "animal"), invisibles = new Property<>(true, "Invisibles");
    private final NumberProperty<Integer> delay = new NumberProperty<>(600, 10, 1500, "Delay", "d"), ticksExisted = new NumberProperty<>(51, 0, 100, "Ticks-Existed", "te", "ticks");
    private final NumberProperty<Float> range = new NumberProperty<>(4F, 3F, 6.5F, "Range", "reach", "r", "distance", "dist");

    public KillAura() {
        super(new String[]{"Kill Aura", "aura", "ka", "ff"}, true, 0xFFFF2B2B, Category.COMBAT);
        this.offerProperties(player, friendProtect, invisibles, lockview, animals, monsters, delay, ticksExisted, range);
        this.addListeners(new Listener<MotionUpdateSubject>("kill_aura_motion_update_listener") {
            @Override
            public void call(MotionUpdateSubject subject) {
                switch (subject.getTime()) {
                    case PRE:
                        gatherTargets();
                        entities.forEach(entity -> target = entity);
                        if (isValidTarget(target)) {
                            float[] rotations = getRotations(target);
                            if (lockview.getValue()) {
                                minecraft.thePlayer.rotationYaw = rotations[0];
                                minecraft.thePlayer.rotationPitch = rotations[1];
                            } else {
                                subject.setRotationYaw(rotations[0]);
                                subject.setRotationPitch(rotations[1]);
                            }
                        } else {
                            entities.remove(target);
                            target = null;
                        }
                        break;
                    case POST:
                        if (isValidTarget(target)) {
                            attack(target);
                            target = null;
                        } else {
                            entities.remove(target);
                            target = null;
                        }
                        break;
                }
            }
        });
    }

    private void attack(EntityLivingBase entityLivingBase) {
        if (stopwatch.reach(delay.getValue())) {
            minecraft.playerController.attackEntity(minecraft.thePlayer, entityLivingBase);
            minecraft.thePlayer.swingArm(EnumHand.MAIN_HAND);
            stopwatch.reset();
        }
    }

    @Override
    protected void onDisable() {
        super.onDisable();
        target = null;
        stopwatch.reset();
        entities.clear();
    }

    private void gatherTargets() {
        for (Object object : minecraft.theWorld.loadedEntityList) {
            Entity entity = (Entity) object;
            if (entity instanceof EntityLivingBase) {
                EntityLivingBase entityLivingBase = (EntityLivingBase) entity;
                if (isValidTarget(entityLivingBase))
                    entities.add(entityLivingBase);
            }
        }
    }

    private boolean isValidTarget(EntityLivingBase entityLivingBase) {
        if (entityLivingBase == null)
            return false;
        if (entityLivingBase.equals(minecraft.thePlayer))
            return false;
        if (entityLivingBase.ticksExisted < ticksExisted.getValue())
            return false;
        if (entityLivingBase.isDead)
            return false;
        if (minecraft.thePlayer.getDistanceToEntity(entityLivingBase) > range.getValue())
            return false;
        if (entityLivingBase instanceof IAnimals && animals.getValue())
            return true;
        if (entityLivingBase instanceof IMob && monsters.getValue())
            return true;
        if (entityLivingBase instanceof EntityPlayer) {
            EntityPlayer entityPlayer = (EntityPlayer) entityLivingBase;
            if (friendProtect.getValue() && Solus.getInstance().getFriendManager().isFriend(entityPlayer.getName()))
                return false;
            if (entityPlayer.capabilities.isCreativeMode)
                return false;
            if (entityPlayer.isInvisible() && !invisibles.getValue())
                return false;
            return player.getValue();
        }
        return false;
    }

    private float[] getRotations(EntityLivingBase entity) {
        final double var4 = entity.posX - minecraft.thePlayer.posX;
        final double var6 = entity.posZ - minecraft.thePlayer.posZ;
        final double var8 = entity.posY + entity.getEyeHeight() / 1.3 - (minecraft.thePlayer.posY + minecraft.thePlayer.getEyeHeight());
        final double var14 = MathHelper.sqrt_double(var4 * var4 + var6 * var6);
        final float yaw = (float) (Math.atan2(var6, var4) * 180.0D / Math.PI) - 90.0F;
        final float pitch = (float) -(Math.atan2(var8, var14) * 180.0D / Math.PI);
        return new float[]{yaw, pitch};
    }
}
