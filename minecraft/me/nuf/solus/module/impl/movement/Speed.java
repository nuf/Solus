package me.nuf.solus.module.impl.movement;

import me.nuf.solus.module.Category;
import me.nuf.solus.module.ToggleableModule;
import me.nuf.solus.properties.NumberProperty;
import me.nuf.solus.properties.Property;
import me.nuf.solus.subjects.TickSubject;
import me.nuf.subjectapi.Listener;

/**
 * Created by nuf on 3/19/2016.
 */
public final class Speed extends ToggleableModule {

    private final Property<Boolean> autoSprint = new Property<>(true, "AutoSprint", "as", "sprint"), fast = new Property<>(true, "Fast");
    private final NumberProperty<Double> speed = new NumberProperty<>(1.5D, 1.1D, 5D, "Speed", "s");

    public Speed() {
        super(new String[]{"Speed", "sprint", "fastrun"}, true, 0xFFFF6947, Category.MOVEMENT);
        this.offerProperties(autoSprint, fast, speed);
        this.addListeners(new Listener<TickSubject>("speed_tick_listener") {
            @Override
            public void call(TickSubject subject) {
                if (autoSprint.getValue())
                    minecraft.thePlayer.setSprinting(canSprint());

                if (fast.getValue())
                    if (minecraft.thePlayer.onGround && !minecraft.thePlayer.isInWater() && !minecraft.thePlayer.isInLava() && (minecraft.gameSettings.keyBindLeft.isKeyDown() || minecraft.gameSettings.keyBindForward.isKeyDown() || minecraft.gameSettings.keyBindBack.isKeyDown() || minecraft.gameSettings.keyBindRight.isKeyDown())) {
                        minecraft.thePlayer.motionX *= speed.getValue();
                        minecraft.thePlayer.motionZ *= speed.getValue();
                    }
            }
        });
    }

    private boolean canSprint() {
        return !minecraft.thePlayer.isCollidedHorizontally && minecraft.thePlayer.moveForward > 0;
    }
}
