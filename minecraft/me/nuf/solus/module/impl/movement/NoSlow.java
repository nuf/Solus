package me.nuf.solus.module.impl.movement;

import me.nuf.solus.module.Category;
import me.nuf.solus.module.ToggleableModule;
import me.nuf.solus.subjects.ItemUseSubject;
import me.nuf.subjectapi.Listener;

/**
 * Created by nuf on 3/21/2016.
 */
public final class NoSlow extends ToggleableModule {
    public NoSlow() {
        super(new String[]{"NoSlow", "noslowdown", "ns"}, true, 0xFFFFC852, Category.MOVEMENT);
        this.addListeners(new Listener<ItemUseSubject>("no_slow_item_use_listener") {
            @Override
            public void call(ItemUseSubject subject) {
                subject.setSpeed(1.7F);
            }
        });
        setEnabled(true);
    }
}
