package me.nuf.solus.module.impl.render;

import me.nuf.solus.module.Category;
import me.nuf.solus.module.ToggleableModule;
import me.nuf.solus.subjects.ChatMessageSubject;
import me.nuf.subjectapi.Listener;

/**
 * Created by nuf on 3/20/2016.
 */
public final class NameProtect extends ToggleableModule {
    public NameProtect() {
        super(new String[] {"NameProtect", "protect", "np"}, false, 0xFF660000, Category.RENDER);
        this.addListeners(new Listener<ChatMessageSubject>("name_protect_chat_message_listener") {
            @Override
            public void call(ChatMessageSubject subject) {
                subject.setCanceled(true);
            }
        });
        setDrawn(false);
        setEnabled(true);
    }
}
