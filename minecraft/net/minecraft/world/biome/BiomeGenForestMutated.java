package net.minecraft.world.biome;

import net.minecraft.world.gen.feature.WorldGenAbstractTree;

import java.util.Random;

public class BiomeGenForestMutated extends BiomeGenForest {
    public BiomeGenForestMutated(BiomeGenBase.BiomeProperties properties) {
        super(BiomeGenForest.Type.BIRCH, properties);
    }

    public WorldGenAbstractTree genBigTreeChance(Random rand) {
        return rand.nextBoolean() ? BiomeGenForest.field_150629_aC : BiomeGenForest.field_150630_aD;
    }
}
