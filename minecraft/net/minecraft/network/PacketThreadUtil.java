package net.minecraft.network;

import me.nuf.solus.core.Solus;
import me.nuf.solus.subjects.PacketSubject;
import net.minecraft.util.IThreadListener;

public class PacketThreadUtil {
    public static <T extends INetHandler> void checkThreadAndEnqueue(final Packet<T> packetIn, final T processor, IThreadListener scheduler) throws ThreadQuickExitException {
        PacketSubject packetSubject = new PacketSubject(packetIn);
        Solus.getInstance().getSubjectManager().dispatch(packetSubject);
        if (packetSubject.isCanceled())
            return;
        if (!scheduler.isCallingFromMinecraftThread()) {
            scheduler.addScheduledTask(new Runnable() {
                public void run() {
                    packetSubject.getPacket().processPacket(processor);
                }
            });
            throw ThreadQuickExitException.INSTANCE;
        }
    }
}
