package net.minecraft.client.gui.spectator;

import net.minecraft.util.text.ITextComponent;

import java.util.List;

public interface ISpectatorMenuView {
    List<ISpectatorMenuObject> func_178669_a();

    ITextComponent func_178670_b();
}
